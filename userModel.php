<?php
require_once("dbconfig.php");

function getUserProfile($ID, $passWord) {
	global $db;
	$sql = "SELECT name, role  FROM user WHERE ID=? and password=?";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "ss", $ID, $passWord); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
			
	if ($row=mysqli_fetch_assoc($result)) {
		//return user profile
		$ret=array('uID' => $ID, 'uName' => $row['name'], 'uRole' => $row['role']);
	} else {
		//ID, password are not correct
		$ret=NULL;
	}
	return $ret;

}

function addUser($id, $password, $name, $status=0){
    global $db;
    $sql = "SELECT count(*) as count FROM user WHERE ID=? ";
	$stmt = mysqli_prepare($db, $sql); //prepare sql statement
	mysqli_stmt_bind_param($stmt, "s", $id); //bind parameters with variables
	mysqli_stmt_execute($stmt);  //執行SQL
    $result = mysqli_stmt_get_result($stmt); //get the results
    $rs = mysqli_fetch_assoc($result);
    if($rs['count'] == 0 ){ // check id 沒有被註冊過(sql count)
        // insert DB
        $sql = "INSERT INTO `user`(`ID`, `password`, `name`, `role`) VALUES (?,?,?,?)";
        $stmt = mysqli_prepare($db, $sql); //prepare sql statement
        mysqli_stmt_bind_param($stmt, "sssi", $id, $password, $name, $status); //bind parameters with variables
        mysqli_stmt_execute($stmt);  //執行SQL
        $result = mysqli_stmt_get_result($stmt); //get the results
        return true;
    }
    else{
        echo "ID已被註冊<br/>";
        return false;
    }
}

?>










